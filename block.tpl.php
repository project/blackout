<?php // $Id$ ?>
<div id="block-<?php echo $block->module . '-' . $block->delta; ?>" class="block block-<?php echo $block->module ?>">
  <?php if ($block->subject): ?>
    <h2 class="title"><?php echo $block->subject ?></h2>
  <?php endif;?>
  <div class="content">
    <?php echo $block->content ?>
  </div>
</div>
